import React, { FC, useState, useEffect, useMemo } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { Autocomplete, CircularProgress, TextField } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const result = await backendClient.getAllUsers();
        setUsers(result.data);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const handleInputChange = (event: any) => {
    const value = event.target.value as string;
    setUsers(users.filter((user: IUserProps) => user.first_name.toLocaleLowerCase().includes(value.toLocaleLowerCase())))
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading  && !users ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
            <>
              <div>
                <Autocomplete
                  options={[]}
                  renderInput={ (params) => (
                    <TextField
                      {...params}
                      label="Search input"
                      InputProps={{
                        ...params.InputProps,
                        type: 'search',
                      }}
                    />
                  )
                  } 
                  onInputChange={(event) => handleInputChange(event)}
                />
              </div>
              <div>
                {users.length
                  ? users.map((user) => {
                      return <UserCard key={user.id} {...user} />;
                    })
                  : null}
              </div>
            </>
        )}
      </div>
    </div>
  );
};
